from tensorrtserver.api import *
import numpy as np
from sklearn.datasets import make_classification

# Some parameters
outputs = 2
batch_size = 1

# Init client
trt_host = '192.168.1.104:8000' # local or remote IP of TRT Server
model_name = 'model_1'
ctx = InferContext(trt_host, ProtocolType.HTTP, model_name)

X,_ = make_classification(2, 5)
print("X: ", X)

# Sample some random data
#data = np.float32(np.random.normal(0, 1, [1, 5]))
#data_0 = np.float32([-1.0249865,  -0.6955977,   0.9224455,  -0.12420372,  0.8941362 ])
data = []
data.append(np.float32(X))
#print(data)
# Get prediction
# Layer names correspond to the names in config.pbtxt
response = ctx.run(
    {'dense_1_input': data}, 
    {'dense_2_output': (InferContext.ResultFormat.CLASS, outputs)},
    batch_size)

# Result
print(response)
