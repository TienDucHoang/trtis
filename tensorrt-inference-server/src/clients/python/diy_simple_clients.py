import os
import numpy as np
from builtins import range
from tensorrtserver.api import *

if __name__ == "__main__":
    protocol = ProtocolType.from_str('http')
    model_name = "simple"
    model_version = -1
    batch_size = 1
    ctx = InferContext("localhost:8000", protocol, model_name, model_version, True)

    input0_data = np.arange(start=0, stop=16,dtype=np.int32)
    input1_data = np.ones(shape=16, dtype=np.int32)
    print("input0_data: ", input0_data)
    print("input1_data: ", input1_data)

    result = ctx.run({'INPUT0' : (input0_data, ),
                    'INPUT1' : (input1_data, )},
                    {'OUTPUT0' : InferContext.ResultFormat.RAW,
                    'OUTPUT1' : InferContext.ResultFormat.RAW
                    },
                    batch_size)
    output0_data = result['OUTPUT0'][0]
    output1_data = result['OUTPUT1'][0]
    print("result: ", result)    
    for i in range(16):
        print(str(input0_data[i]) + " + " + str(input1_data[i]) + " = " + str(output0_data[i]))
        print(str(input0_data[i]) + " - " + str(input1_data[i]) + " = " + str(output1_data[i]))
        if (input0_data[i] + input1_data[i]) != output0_data[i]:
            print("error: incorrect sum");
            sys.exit(1);
        if (input0_data[i] - input1_data[i]) != output1_data[i]:
            print("error: incorrect difference");
            sys.exit(1);